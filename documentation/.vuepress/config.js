module.exports = {
    title: 'Frontend Vue developer questions',
    head: [
        ['link', { rel: 'icon', href: '/1_4eqCyxw5ZWuSUBLv5g8i6g.png' }]
    ],
    themeConfig: {
        sidebar: [
            ['/js-interview/', 'Базовые вопросы JS'],
            ['/css-interview/', 'Базовые css, scss'],
            ['/js-advanced-interview/', 'Продвинутые вопросы JS'],
            ['/js-practice-tasks/', 'Практические задания JS'],
            ['/vue-interview/', 'Базовые вопросы Vue'],
            ['/vue-router-interview/', 'Базовые вопросы Vue Router'],
            ['/vuex-interview/', 'Базовые вопросы Vuex'],
            ['/vue-advanced-interview/', 'Продвинутые вопросы Vue'],
        ]
    },
    base: '/frontend-vue-interview/'
}
