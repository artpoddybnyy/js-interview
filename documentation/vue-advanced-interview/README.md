# Vue Advanced
1. Существует ситуация когда нужно асинхронно из хранилища полуить данные и добавить в компонент если данных там еще нет
Данная ситуация приводит к тому что необхоимо выполнить такой код при иницилизации компонента
```javascript
created() {
    this.compoData = this.store.getters('someGetter')
}
```
и затем при добавлении данных в хранилище необходимо за ними проследить
```javascript
watch: {
    'store.getters' (newValue) {
          this.compoData = newValue
    }
}
```
:::tip
необходимо сделать ваш ватчер immediate true
```javascript
'store.getters': {
    heandler (newValue) {
      this.compoData = newValue
    }, 
    immediate: true
}
```
:::
2. Существует ситуация когда нужно при инициализации компонента к примеру подписать на событие DOM а перед тем когда компонент будет унечтожен отписаться от события
Это возмоэно сделать через доболнительную переменную коммпонента в которую можно сохранить подписку а затем делать отписку в нужный момент.
:::tip
 Более компактное решение:
```javascript
created () {
   const escapeHandler = e => {
      if (e.key === 'Escape' && this.show) {
        this.cancel()
      }
    }
    document.addEventListener('keydown', escapeHandler)
    this.$once('hook:destroyed', () => {
      document.removeEventListener('keydown', escapeHandler)
    })
}
```
:::
3. Как можно в асинхронном коде отображать Vue экземпляр, к примеру нужно проинецелизировать какие то данные которые приходять с сервера и затем только рендерить екземпляр Vue
:::tip
```javascript
var app = new Vue({
//modules
})
setTimeout(() => app.$mount('#app'), 1000)
```
:::

4. Как слушать хуки дочернего компонента
:::tip
```javascript
<component @hook:created="someFunc" />
```
* Прослушивание хуков внутри компонента
```javascript
 this.$on('hook:mounted', function () {
       // прослушивает постоянно
})
this.$once('hook:mounted', function () {
       // прослушивает один раз и отписываеться
})

```
:::
###### props vs v-model link
5. Cинхранизировать props vs v-model 
:::tip
* v-model
```javascript
const vModelComponent = {
      template: `
       <template> 
          <div> 
            <label for="checkbox">checkbox</label>
            <input type="checkbox" id="checkbox" :checked="checked"
            @change="$emit('change', $event.target.checked)"  >
          </div>
       </section>     
       `,
        model: {
          prop: 'checked',
          event: 'change'
        },
        props: {
          checked: Boolean,
      }
}
```
##### Somewhere in you code
```javascript
 <vModelComponent v-model="checked"/>
```
* props sync

```javascript
const SyncPropsComponent = {
   template: `
    <template> 
      <input :value="size" @input="$emit('update:size', $event.target.value.toUpperCase())" />
    </template> 
   `,
   props: {
       size: String
   }
}
```
##### Somewhere in you code
```javascript
<SyncPropsComponent :size.sync="size"></SyncPropsComponent>
```
:::
6. Как дебажить код внутри `<tamplate></tamplate>`
::: tip
```html
<h1>{{(function(){debugger; message)() || message}}</h1>
```
:::
7. Как перестать отслеживать изменения которые делаються через `watch` или сделать `watch` единажды
::: tip
* необходтмо задать правельный `watch`
```javascript
 var unwatch = this.$watch('value', function(value) {
    unwatch();
});
```
* Что не так с эти кодом ?
```javascript
    var unwatch = this.$watch('value', function(value) {
        console.log('once immediate', value);
        unwatch();
      }, { immediate: true }); // immediate не работает
```
:::
8. Как работают `scoped` стили? Что будет в следующем примере и как это исправить
::: tip
* Какой то компонент в котором используеться `BaseList` в котором есть класс `.list-item`
```html
<template>
  <BaseList :items="items" />
</template>
<style scoped>
   .list-item {
     color: white;
     background: red;
    }
</style>

```
* Какой то другой компонент в котором используеться `BaseList` в котором есть класс `.list-item`
```html
<template>
  <BaseList :items="items" />
</template>
<style scoped>
   .list-item {
     color: white;
     background: green;
    }
</style>
```
Исправить все можно добавив /deep/ .list-item {} каждому из селекторов
:::
9. Методы массивов которые поддерживают реактивность
```javascript
push()
pop()
shift()
unshift()
splice()
sort()
reverse()
```
