1. Как отображать картинки base64 
 ::: tip
 * `background-image: url("")`, 
 * `<img src="" />` 
 * `data:image/png;base64, код`
 :::
 2. Какую анимацию и как использовать  **transition**, **animation** в чем отличие, можно ли остановить анимацию. 
 Что такое **transform**, **transform-origin**. **transform-origin** у **svg**
 ::: tip
 * `animation-play-state:paused;`
 * transform-origin у svg 0% 0% по умолчанию
 :::
 3. Что такое **svg** и как анимировать **svg**
 4. Как применять к элементу одни и теже с тиле но у каждого елемента будет разный шрифт
 ```javascript
 .style-one, style-two {
    //styles
 }
 style-two { 
  //special styles
 }
 ```
 5. **css** селекторы, спецефичнось селекторов
::: tip
 ```javascript
ul > li { // выбирает элементы, которые являются дочерними непосредственно 
border: 1px solid black;
}
ul ~ p { //выбирает элементы, которые находятся после указанного элемента, 
 color: red;
}
ul + p { //выбирает элемент, который находится непосредственно после указанного элемента
 color: red;
}
input[type=radio]:checked {
 border: 1px solid black;
}
```
:::
 6. **scss** `@for`, `@if`, `@each`, `@while`
 7. Как по ховеру показать скрыть соседний элемент
::: tip
 ```javascript
.showme { 
    display: none;
}
.showhim:hover .showme{
    display: block;
}
```
:::
