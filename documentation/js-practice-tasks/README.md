# Практические задания
1. Написать код который будет находить в массиве минимальное или максимальное значение
2. Написать реализацию следующих методов
```javascript
const initialArr = [2,4,6,8]

const result = multipleMap(initialArr, function (item) { return item * 2 })
result === [4,8,12,16]

const result2 = getSumm(initialArr, function (currentItem, item) { return currentItem + item })
result2 === 20
    
const arrSymbols = ['aa', 'vv', 'ssa']
const resSymbols = getSymbols(arrSymbols, elem => elem.includes('a'))
resSymbols === ["aa", "ssa"]
   
   var arrs = [
        function (param) { return param + 2 },
        function (param) { return param - 2 },
        function (param) { return param * 2 },
        function (param) { return param / 2 }
        ]
result === [5, 3, 6, 3]
var res = 3
arrs.map(fn =>  res = fn(res))
```


3. Написать реализацию функции `sum()` что бы в результате работы кода получилось число 7
 ```javascript
       sum(1);
       sum(2);
       sum(4);
```

4. Написать собственную реализацию что бы код работал
```javascript
let observable = new Observable();
let observer = new Observer();

observable.addObserver(observer);

setInterval(function () {
  observable.setState({ val: Math.floor(Math.random() * (10 - 1)) + 1 })
}, 1000);

observer.subscribe(function (data) {
  if (data.val > 5) {
    console.log('>5', data)
  }
})

observer.subscribe(function (data) {
  if (data.val < 5) {
    console.log('<5', data)
  }
})

observer.subscribe(function (data) {
  if (data.val === 5) {
    console.log('=5', data)
  }
})
```
5. Написать собственную реализации функции или класса **MyPromise**
```javascript
function getUserAddress() {
  return new MyPromise((resolve, reject) => {
    setTimeout(function() {
       resolve('data')
    }, 1000)
  })
}
getUserAddress().then(data => {
  console.log(data)
})
```
6. Как нативно получить квери параметр с url (new URLSearchParams)
7. Написать свой async await
```javascript
async(function* () {
    var res = yield getAsyncData()
    //some ations
    var res2 = yield getAsyncData()
       //some ations
})
``` 
8. Сделать из массива объектов а котором есть повторяющиеся объекты массив уникальных объектов

```javascript
var a =[{name:'John'}, {name: 'Mike'}, {name: 'Mike'}, {name: 'John'}]
var w = new Map()
a.forEach(elem => w.set(elem.a, elem))
```
9. Написать функцию которая будет проверять строку
10. Написать свой `switch`
::: tip
```javascript
function switchCase(value, obj) {
 return obj[value]
}
switchCase('b', {'a': 10, 'b': 20, 'c': 30}) 

function switchCase2(res) {
    return {
        [res === 'b' || res === 'a']: 'a or b',
        [res === 'c']: 'cccc',
    }
}
switchCase2('a')['true']
```
:::
11. Написать код который будет менять местами имя фамилию в строке
```javascript
var name = 'Jone Doe'

res === 'Doe Jone'
```
::: tip
   ```javascript
  name.replace(/([a-z]+)\s([a-z]+)/i, '$2 $1')
   ```
:::
12. Как выполниться следующий код
```javascript
async function test(){
    await Promise.resolve()
    console.log('test executed')
}
setTimeout(()=> console.log('timeout executed'))
test()
```
13. Как отработает код
```javascript
function pr() {
    return new Promise(function(res, rej){
        reject()	
        resolve()  
    })
}
pr().then(function(){
    console.log(1)
}).catch(function(){
    console.log(2)
})
```
14. Написать сортировку массива состоящий из строк и цифр `['80', '9', '700', 40, 1, 5, 200]`
```javascript
 ['80', '9', '700', 40, 1, 5, 200].sort(function(a, b) {
  return a - b;
})
```
15. Посчитать сумму элементов массива `['80', '9', '700', 40, 1, 5, 200]`
```javascript
['80', '9', '700', 40, 1, 5, 200].reduce(function(a, b){
    return a + +b
    }, 0)
```
16. Как безопасно конкотенировать строки 
```javascript
var one = 1;
var two = 2;
var three = '3';

var result = ''.concat(one, two, three); //"123"
```
17. Как передать в callback свои пользовательские параметры.
Например на елементе находиться 2 обработчика событий левый и правый клик, функция обработчик
выполняет один и тотже фнкционал скролинга страници на правый клик на 100 пикселей на левый клик 200 пикселей
```javascript
var doc = document.getElementsByTagName('body')[0]
doc.addEventListener('click', callback('a', 2));
function callback (a, b) {
	return function (e) {
        console.log(a , b)
		console.log(e)
    }
}
```
