# JS интевью advanced 
1. Что такое **window** и **document**
2. Что такое атрибуты у ключа объекта
```javascript
Object.defineProperty(obj, 'key', {
    enumerable: false,
    configurable: false,
    writable: false,
    value: 'static'
    });
```
3. Где лучше хранить данные на стороне клиента 
::: tip
**indexDB**, **local storage**, **websql**
:::
4. Как синхронизировать две вкладки
::: tip
слушать **localstorage**, или использовать **postMessage**
на одной той же вкладке **localstorage** нельзя слушать
:::
5. Какие события можно слушать на input, отличе **blur** и **focusout**
::: tip
у **blur** нет дальнейшего всплытия
:::
7. Как отследить анимацию **transition**, **animation** (начало, окончание)
8. Тег `<pre>`, что он делает
9. Тег `<sript>`, **defer** и атрибуты 
::: tip
* **defer** откладывает на конец загрузки документа
* **async** документ не ожидает загрузку скрипта
:::
10. Что такое **srcset**, **dataset** атрибуты на елементах
11. Как выровнять елемнт в контейнере по вертикали и горизонтали
12. Как открыть почтовый клиент 
::: tip
`<a href="mailto:vlad@htmlbook.ru?subject=Вопрос по HTML">`
:::
13. Приоритет индексирования анимации **transform** и z-index
14. Что такое **Web Workers API**, для чего нужны **Web Workers**
15. Можно ли открыть или закрыть окно JS'ом
 ::: tip
 * window.open() **(не работает в асинхронной опирации)**
 * window.close()
 :::
17. Что такое пассивное прослушивание событий
18. Как очистить массив
19. Что такое SVG, как анимировать. Что такое `stroke-dashoffset` `stroke-dasharray`
20. Почему в классе методы определяються не в конструкторе 
```
Функция, определенная в классе (не в конструкторе), находится в прототипе класса, с которым связан 
каждый экземпляр. Функции, определенные в конструкторе, становятся собственными свойствами каждого экземпляра. 
Если вы определили функцию в конструкторе, каждый экземпляр получит свою собственную копию функции. 
Если вы этого не сделаете, экземпляры будут отложены до цепочки прототипов, и все экземпляры будут указывать на одну и ту 
же функцию.
```
21. Как сделать из флоат инт `var a = 22.123123 | 0`
22. Взять последние елементы массива `array.slice(-3)`
23. Как пропустить параметр в функции `fc(...[param_1, , param_2])`
24. Как сравнить 2 значения если потенциально они могут быть `NaN` `Object.is(NaN, NaN);`
25. Создать массив на 50 интов `Array.from(Array(50).keys());`
26. Как создать объкт обеспечив очередь втавки елементов `new Map().set()`
